# Artifaker Imagineer

### Summary

Artifaker Imagineer is a image gallery web application which allows people sharing their images and creative tales in different categories. 
Users are expected to write a story related to the category and the image for the  images which is sent randomly to the predetermined categories that represent different medium.

### Motivation

The purpose of the project is making different platform encouraging creative thinking.

### Tools

Django 3.0

Bootstrap 4.5

## Demonstration

[Live demo is available in pythonanywhere](https://artifakerimagineer.pythonanywhere.com/)

![](snapshot.png)






