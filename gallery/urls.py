from django.urls import path, include
from django.views.generic.base import RedirectView
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('category/<str:category>', views.category, name='category'),
    path('review/<str:image>', views.review, name='review'),
    path('image-upload', views.imageupload, name='imageupload'),
]
